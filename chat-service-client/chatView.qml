import QtQuick 2.15
import QtQuick.Controls 2.12
import Enums 1.0
Item {

    function sendMessage()
    {
        client.onSendClicked(messageText.text)
        messageText.text = ""
    }
    BusyIndicator
    {
        running: client.isConnecting;
        anchors.centerIn: parent
    }
    Column
    {
        id: chatViewColumn
        spacing: 10
        anchors.fill: parent

        padding: 20
        topPadding: 40

        property var messageRowHeight: 40
        property var sendBtnWidth: 40

        ListView
        {
            id: messageListView
            width: parent.width - parent.padding
            height: parent.height - parent.messageRowHeight - parent.spacing - (parent.topPadding * 2)
            spacing: 15
            model: client.messages

            delegate: TextEdit
            {
                selectByMouse: true
                color:
                {
                    if (modelData.type === Enums.MESSAGE) return "white"
                    else if (modelData.type === Enums.INFO || modelData.type === Enums.CONNECT) return "yellow"
                    else return "red"
                  }
                readOnly: true
                textFormat: TextEdit.RichText
                wrapMode: Text.WordWrap
                width: parent.width

                text: {
                    if (modelData.type === Enums.MESSAGE) return "<i><font color=\"#5555FF\">" + modelData.username + "</font></i>" + " : " + modelData.message
                    else if (modelData.type === Enums.INFO || modelData.type === Enums.CONNECT) return "<i>" + modelData.message + "</i>"
                    else return "<b>" + modelData.message + "</b>"
                  } }

            onCountChanged:
            {
                Qt.callLater( messageListView.positionViewAtEnd )
            }
        }

        Row
        {
            enabled: !client.isConnecting
            height: parent.messageRowHeight
            width: parent.width
            leftPadding: 20
            rightPadding: 20
            TextField
            {
                height: parent.height
                width: parent.width - chatViewColumn.sendBtnWidth - parent.leftPadding - parent.rightPadding
                id: messageText

                onAccepted:
                {
                    if(text.length > 0)
                    {
                        sendMessage()
                    }
                }
            }

            Button
            {
                height: parent.height
                width: chatViewColumn.sendBtnWidth
                enabled: messageText.length > 0
                text: ">>"
                onClicked:
                {
                    sendMessage()
                }
            }
        }

        Component.onCompleted:
        {
            for (var item in children)
                children[item].anchors.horizontalCenter = chatViewColumn.horizontalCenter;
        }
    }

    Shortcut {
        sequence: "Escape"
        context: Qt.WindowShortcut
        onActivated:
        {
            client.onDisconnectClicked();
            stack.pop();
        }
    }
}


