#include "message.h"

const QChar Message::SEPARATOR = '\n';

Message::Message()
{}

Message::Message(const QString& networkString)
{
    try
    {
        setValuesFromNetworkString(networkString);
    }
    catch(const std::exception& e)
    {
        _type = ENUMS::ERROR;
        _message = "error: " + QString(e.what());
    }
}

void Message::setUsername(const QString &username)
{
    _username = "";
    QStringList list = username.split(Message::SEPARATOR);

    //removing separetor symbols, if username contains them
    for(const auto& text: list)
    {
        _username += text;
    }
}

void Message::setMessage(const QString &message)
{
    _message = "";
    QStringList list = message.split(Message::SEPARATOR);

    //removing separetor symbols, if message contains them
    for(const auto& text: list)
    {
        _message += text;
    }
}

void Message::setType(const ENUMS::MessageType& type)
{
    _type = type;
}

QString Message::generateNetworkString() const
{
    return QString(QString::number(_type) + SEPARATOR + _username + SEPARATOR + _message).toUtf8();
}

void Message::setValuesFromNetworkString(const QString& networkString)
{
    QStringList stringlist = networkString.split(SEPARATOR);

    if(stringlist.length() != 3)
    {
        throw std::runtime_error("malformed message");
    }

    _type = static_cast<ENUMS::MessageType>(stringlist[0].toInt());
    _username = stringlist[1];
    _message =stringlist[2];
}
