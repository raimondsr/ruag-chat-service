import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import Client 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Chat Client")
    color: "black"

    StackView {
        id: stack
        initialItem: (Qt.createComponent(Qt.resolvedUrl("connectMenu.qml")).createObject())
        anchors.fill: parent

    }

    Client
    {
        id: client
    }


}
