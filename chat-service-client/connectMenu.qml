import QtQuick 2.15
import QtQuick.Controls 2.12

Item {
    function connect()
    {
        client.onConnectClicked(username.text, host.text, port.text);
        stack.push(Qt.createComponent(Qt.resolvedUrl("chatView.qml")).createObject())
    }

    Column
    {
        anchors.fill: parent
        id: serverMenuColumn
        spacing: 20

        topPadding: 40

        TextField
        {
            id: username
            width: parent.width / 2
            placeholderText : qsTr("Name")

            onAccepted:
            {
                if(text.length > 0)
                {
                    connect()
                }
            }
        }

        TextField
        {
            id: host
            width: parent.width / 3
            placeholderText : qsTr("Host")
            text: "localhost"
            onAccepted:
            {
                if(text.length > 0)
                {
                    connect()
                }
            }
        }
        TextField
        {
            id: port
            width: parent.width / 6
            placeholderText : qsTr("Port")
            text: "2222"
            onAccepted:
            {
                if(text.length > 0 && host.text.length != 0 && port.text.length != 0)
                {
                    connect()
                }
            }
        }

        Button
        {
            text: qsTr("Connect")
            enabled: username.text.length != 0 && host.text.length != 0 && port.text.length != 0
            onClicked:
            {
                connect()
            }
        }

        Component.onCompleted:
        {
            for (var item in children)
                children[item].anchors.horizontalCenter = serverMenuColumn.horizontalCenter;
        }
    }
}
