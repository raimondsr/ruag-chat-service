#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>
#include <stdexcept>

namespace ENUMS
{
    Q_NAMESPACE
    enum MessageType {
        MESSAGE,
        INFO,
        ERROR,
        CONNECT
    };
    Q_ENUM_NS(MessageType)
}

class Message
{
private:
    Q_GADGET
    Q_PROPERTY(QString username MEMBER _username)
    Q_PROPERTY(QString message MEMBER _message)
    Q_PROPERTY(ENUMS::MessageType type MEMBER _type)

    QString _username = "";
    QString _message = "";
    ENUMS::MessageType _type;

    static const QChar SEPARATOR;

public:

    Message();
    Message(const QString& networkString);


    QString getUsername()           const {return _username;}
    QString getMessage()            const {return _message;}
    ENUMS::MessageType getType()    const {return _type;}

    void setUsername(const QString& username);
    void setMessage(const QString& message);
    void setType(const ENUMS::MessageType& type);

    QString generateNetworkString() const;
    void setValuesFromNetworkString(const QString& networkString);

};

#endif // MESSAGE_H
