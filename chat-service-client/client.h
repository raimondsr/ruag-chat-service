#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include<QVariantList>
#include <QDebug>
#include<QTcpSocket>

#include "message.h"


class Client : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantList messages READ messages NOTIFY messagesChanged)
    Q_PROPERTY(bool isConnecting READ isConnecting NOTIFY connectionChanged)

public:

    Client(QObject *parent = nullptr);
    QVariantList messages() const;
    bool isConnecting() const {return _isConnecting;}

public slots:
    void onSendClicked(const QString& message);
    void onConnectClicked(const QString& username,const QString& host,const QString& port);
    void onConnected();
    void onDisconnectClicked();
    void onDataRecieved();
    void showError();

signals:
    void messagesChanged();
    void connectionChanged();

private:

    QTcpSocket* _socket;
    QVector<Message> _messages;
    QString _username = "";
    bool _isConnecting = false;

    void _setUsername(const QString& username);
};


#endif // CLIENT_H
