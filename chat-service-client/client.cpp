#include "client.h"


Client::Client(QObject *parent) : QObject(parent)
{}

QVariantList Client::messages() const
{
    QVariantList result;

    for(const auto& msg: _messages)
    {
        result << QVariant::fromValue(msg);
    }

    return result;
}


void Client::onConnectClicked(const QString &username,const QString &host,const QString &port)
{
    _isConnecting = true;
    emit connectionChanged();

    _setUsername(username);
    _username = username;

    _socket = new QTcpSocket(this);
    connect(_socket, &QTcpSocket::connected, this, &Client::onConnected);
    connect(_socket, &QTcpSocket::readyRead, this, &Client::onDataRecieved);
    connect(_socket, SIGNAL(error(QAbstractSocket::SocketError)),this, SLOT(showError()));

    _socket->connectToHost(host, port.toInt());
}

void Client::_setUsername(const QString& username)
{
    _username = username;
}

void Client::showError()
{
   Message msg;
   msg.setType(ENUMS::ERROR);
   msg.setMessage(_socket->errorString());
   _messages.append(msg);
   emit messagesChanged();
}

void Client::onConnected()
{
    _isConnecting = false;
    emit connectionChanged();

    Message msg;
    msg.setType(ENUMS::CONNECT);
    msg.setUsername(_username);
    msg.setMessage(_username + " connected");
    _socket->write(msg.generateNetworkString().toUtf8());
}

void Client::onDisconnectClicked()
{
    _socket->disconnect();
    _socket->deleteLater();
}

void Client::onDataRecieved()
{
    QByteArray data = _socket->readAll();
    Message msg(data);
    _messages.push_back(msg);
    emit messagesChanged();
}

void Client::onSendClicked(const QString &message)
{
    Message msg;
    msg.setType(ENUMS::MESSAGE);
    msg.setUsername(_username);
    msg.setMessage(message);
    _socket->write(msg.generateNetworkString().toUtf8());
    _messages.append(msg);
    emit messagesChanged();
}


