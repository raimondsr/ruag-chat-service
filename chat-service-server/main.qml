import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import Server 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Chat Server")
    Row
    {
        anchors.fill: parent
        Item {
            width: parent.width/2
            height: parent.height;
            Button
            {
                anchors.centerIn: parent
                text: server.isRunning ? "Stop Server" : "Start Server"
                onClicked:
                {
                    if(server.isRunning)
                    {
                        server.onServerStopClicked();
                    }
                    else
                    {
                        server.onServerStartClicked()
                    }
                }
            }
        }

        ListView
        {
            id: userList
            width: parent.width/2
            height: parent.height
            spacing: 15
            model: server.users
            delegate: Text
            {
                text: modelData
            }
        }

    }
    Server
    {
        id:server
    }

}
