#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QPointer>
#include "../chat-service-client/message.h"
#include <QStringList>

struct Connection
{
    QString username = "";
    QPointer<QTcpSocket> socket;
    bool ready = false;
};

class Server : public QTcpServer
{
    Q_OBJECT

    Q_PROPERTY(QStringList users READ users NOTIFY usersChanged)
    Q_PROPERTY(bool isRunning READ isRunning NOTIFY runningChanged)

public:
    explicit Server(QObject *parent = nullptr);

    bool isRunning() const {return _isRunning;}
    QStringList users() const;

public slots:
    void onServerStartClicked();
    void onServerStopClicked();
    void onNewConnection();
    void dataRecieved();
    void onDisconnected();
signals:
    void runningChanged();
    void usersChanged();
private:

    bool _isRunning = false;
    QVector<Connection> _activeConnections;

};

#endif // SERVER_H
