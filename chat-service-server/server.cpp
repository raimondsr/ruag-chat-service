#include "server.h"

bool operator == (const Connection &c1, const Connection &c2)
{
    return c1.socket == c2.socket;
}

Server::Server(QObject *parent) : QTcpServer(parent)
{
    connect(this, SIGNAL(newConnection()),this, SLOT(onNewConnection()));
}


void Server::onServerStartClicked()
{
    listen(QHostAddress::Any, 2222);
    qDebug()<< serverAddress() << " : "<< serverPort();
    _isRunning = true;
    emit runningChanged();
}

void Server::onServerStopClicked()
{
    close();
    qDebug()<<"server closed";
    for(auto& connection : _activeConnections)
    {
        connection.socket->deleteLater();
    }
    _activeConnections.clear();
    _isRunning = false;

    emit runningChanged();
    emit usersChanged();
}

QStringList Server::users() const
{
    QStringList userList;
    for(const auto& connection: _activeConnections)
    {
        userList.append(connection.username);
    }
    return userList;
}

void Server::onNewConnection()
{
    qDebug()<<"connection";
    Connection connection;
    connection.socket = nextPendingConnection();
    connect(connection.socket, &QTcpSocket::readyRead, this, &Server::dataRecieved);
    connect(connection.socket, &QTcpSocket::disconnected, this, &Server::onDisconnected);
    _activeConnections.append(connection);
}

void Server::dataRecieved()
{
    qDebug()<<"data";

    QTcpSocket* senderSocket = qobject_cast< QTcpSocket* >(sender());
    QByteArray msgByteArray = senderSocket->readAll();

    Message msg(msgByteArray);

    if(msg.getType() == ENUMS::CONNECT) //Finish connection
    {
        for(auto& connection: _activeConnections) //TODO refactor using find & lambda
        {
            if(connection.socket == senderSocket)
            {
                connection.username = msg.getUsername();
                connection.ready = true;
                emit usersChanged();

                break;
            }
        }
    }

    for(const auto& connection: _activeConnections)
    {
        if(connection.ready && (connection.socket != senderSocket || msg.getType() == ENUMS::CONNECT))
        {
            connection.socket->write(msgByteArray);
        }
    }
}


void Server::onDisconnected()
{
    QTcpSocket* senderSocket = qobject_cast< QTcpSocket* >(sender());
    Connection lostConnection;

    for(auto& connection: _activeConnections) //TODO refactor using find & lambda
    {
        if(connection.socket == senderSocket)
        {
            lostConnection = connection;
            break;
        }
    }

    Message msg;
    msg.setType(ENUMS::INFO);
    msg.setUsername(lostConnection.username);
    msg.setMessage(lostConnection.username + " disconnected");

    QString messageString = msg.generateNetworkString();

    for(const auto& connection: _activeConnections)
    {
        if(connection.ready)
        {
            connection.socket->write(messageString.toUtf8());
        }
    }
    _activeConnections.removeOne(lostConnection);
    lostConnection.socket->deleteLater();
    emit usersChanged();
}
